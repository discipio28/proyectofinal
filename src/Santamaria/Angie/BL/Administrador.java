package Santamaria.Angie.BL;

public class Administrador extends Usuario{

    //solo debe permitir 1 administrador

    public Administrador(String nombre, String primerApellido, String segundoApellido, String identificacion, String pais, String nombreUsuario, String clave) {
        super(nombre, primerApellido, segundoApellido, identificacion, pais, nombreUsuario, clave);
    }

    @Override
    public String toString() {
        return "Administrador{}";
    }
}
