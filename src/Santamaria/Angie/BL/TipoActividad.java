package Santamaria.Angie.BL;

public class TipoActividad {
    private String codigoAutogenerado, nombre, icono;

    public TipoActividad (String codigoAutogenerado, String nombre, String icono) {
        this.codigoAutogenerado = codigoAutogenerado;
        this.nombre = nombre;
        this.icono = icono;
    }

    public String getCodigoAutogenerado() {
        return codigoAutogenerado;
    }

    public void setCodigoAutogenerado(String codigoAutogenerado) {
        this.codigoAutogenerado = codigoAutogenerado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    @Override
    public String toString() {
        return "TipoActividad{" +
                "codigoAutogenerado='" + codigoAutogenerado + '\'' +
                ", nombre='" + nombre + '\'' +
                ", icono='" + icono + '\'' +
                '}';
    }
}
