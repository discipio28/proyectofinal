package Santamaria.Angie.BL;

public class Hitos {
    private String nombre, kmActual, imagen, descripcion, referenciaPagina;

    public Hitos (String nombre, String kmActual, String imagen, String descripcion, String referenciaPagina) {
        this.nombre = nombre;
        this.kmActual = kmActual;
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.referenciaPagina = referenciaPagina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getKmActual() {
        return kmActual;
    }

    public void setKmActual(String kmActual) {
        this.kmActual = kmActual;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getReferenciaPagina() {
        return referenciaPagina;
    }

    public void setReferenciaPagina(String referenciaPagina) {
        this.referenciaPagina = referenciaPagina;
    }

    @Override
    public String toString() {
        return "Hitos{" +
                "nombre='" + nombre + '\'' +
                ", kmActual='" + kmActual + '\'' +
                ", imagen='" + imagen + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", referenciaPagina='" + referenciaPagina + '\'' +
                '}';
    }
}
