package Santamaria.Angie.BL;

public class Retos {
    String codigo, nombreReto, descripcion, medalla, foto;
    double cantidadKm, costoReto, puntoInicio, puntoFinal; // cantidadKm = puntoInicio - puntoFinal

    public Retos(String codigo, String nombreReto, String descripcion, String medalla, String foto, double cantidadKm, double costoReto, double puntoInicio, double puntoFinal) {
        this.codigo = codigo;
        this.nombreReto = nombreReto;
        this.descripcion = descripcion;
        this.medalla = medalla;
        this.foto = foto;
        this.cantidadKm = cantidadKm;
        this.costoReto = costoReto;
        this.puntoInicio = puntoInicio;
        this.puntoFinal = puntoFinal;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombreReto() {
        return nombreReto;
    }

    public void setNombreReto(String nombreReto) {
        this.nombreReto = nombreReto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMedalla() {
        return medalla;
    }

    public void setMedalla(String medalla) {
        this.medalla = medalla;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public double getCantidadKm() {
        return cantidadKm;
    }

    public void setCantidadKm(double cantidadKm) {
        this.cantidadKm = cantidadKm;
    }

    public double getCostoReto() {
        return costoReto;
    }

    public void setCostoReto(double costoReto) {
        this.costoReto = costoReto;
    }

    public double getPuntoInicio() {
        return puntoInicio;
    }

    public void setPuntoInicio(double puntoInicio) {
        this.puntoInicio = puntoInicio;
    }

    public double getPuntoFinal() {
        return puntoFinal;
    }

    public void setPuntoFinal(double puntoFinal) {
        this.puntoFinal = puntoFinal;
    }

    @Override
    public String toString() {
        return "Retos{" +
                "codigo='" + codigo + '\'' +
                ", nombreReto='" + nombreReto + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", medalla='" + medalla + '\'' +
                ", foto='" + foto + '\'' +
                ", cantidadKm=" + cantidadKm +
                ", costoReto=" + costoReto +
                ", puntoInicio=" + puntoInicio +
                ", puntoFinal=" + puntoFinal +
                '}';
    }
}