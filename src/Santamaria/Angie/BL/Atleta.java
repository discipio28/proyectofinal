package Santamaria.Angie.BL;

public class Atleta extends Usuario {

    //TODO: falta metodo de pago y avatar

    private String fechaNacimiento, genero, direccion;

    public Atleta(String nombre, String primerApellido, String segundoApellido, String identificacion, String pais, String nombreUsuario,
                  String clave, String fechaNacimiento, String genero, String direccion) {
        super(nombre, primerApellido, segundoApellido, identificacion, pais, nombreUsuario, clave);
        this.fechaNacimiento = fechaNacimiento;
        this.genero = genero;
        this.direccion = direccion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Atleta{" +
                "fechaNacimiento='" + fechaNacimiento + '\'' +
                ", genero='" + genero + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
