package Santamaria.Angie.BL;

public class MetodoPago {
    int numTarjeta, pin, mesExpiracion, annoExpiracion;
    String proveedor, nombreEnTarjeta;

    public MetodoPago (int numTarjeta, int pin, int mesExpiracion, int annoExpiracion, String proveedor, String nombreEnTarjeta) {
        this.numTarjeta = numTarjeta;
        this.pin = pin;
        this.mesExpiracion = mesExpiracion;
        this.annoExpiracion = annoExpiracion;
        this.proveedor = proveedor;
        this.nombreEnTarjeta = nombreEnTarjeta;
    }

    public int getNumTarjeta() {
        return numTarjeta;
    }

    public void setNumTarjeta(int numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getMesExpiracion() {
        return mesExpiracion;
    }

    public void setMesExpiracion(int mesExpiracion) {
        this.mesExpiracion = mesExpiracion;
    }

    public int getAnnoExpiracion() {
        return annoExpiracion;
    }

    public void setAnnoExpiracion(int annoExpiracion) {
        this.annoExpiracion = annoExpiracion;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getNombreEnTarjeta() {
        return nombreEnTarjeta;
    }

    public void setNombreEnTarjeta(String nombreEnTarjeta) {
        this.nombreEnTarjeta = nombreEnTarjeta;
    }

    @Override
    public String toString() {
        return "MetodoPago{" +
                "numTarjeta=" + numTarjeta +
                ", pin=" + pin +
                ", mesExpiracion=" + mesExpiracion +
                ", annoExpiracion=" + annoExpiracion +
                ", proveedor='" + proveedor + '\'' +
                ", nombreEnTarjeta='" + nombreEnTarjeta + '\'' +
                '}';
    }
}
